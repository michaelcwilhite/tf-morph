variable "table_name" {
  type = string
  default = "<%=customOptions.dynamoTableName%>"
  // default = "test-db"
}

variable "hash_key" {
  type = string
  default = "<%=customOptions.dynamoTableHashKey%>"
  // default = "name"
}

variable "hash_key_type" {
  type = string
  // default = "<%=customOptions.dynamoTableHashKeyType%>"
  default = "S"
}
